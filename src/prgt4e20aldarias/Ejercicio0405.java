/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package prgt4e20aldarias;

/**
 * Fichero: Ejercicio0405.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-nov-2013
 */

public class Ejercicio0405 {

  public static void main(String [] args) {
    for (int i=1; i< 6 ; i++) {
      for (int j=0; j<i; j++) {
        System.out.print("*");
      }
      System.out.println("");
    }
  }
}

/* EJECUCION:
*
**
***
****
*****
*/