/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package prgt4e20aldarias;

import java.util.Random;

/**
 * Fichero: Ejercicio0408.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-nov-2013
 */

public class Ejercicio0408 {

  public static void main(String[] args) {
    Random rnd = new Random();
    int valor = rnd.nextInt();
    valor=(valor%101); // valor entre <=100
    if (valor < 0) { // Es negativo
      valor=-1*valor;
    }
    valor+=100;
    System.out.println(valor);
  }
}
