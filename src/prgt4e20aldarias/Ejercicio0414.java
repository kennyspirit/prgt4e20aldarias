/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt4e20aldarias;

/**
 * Fichero: Ejercicio0414.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 18-nov-2013
 */
public class Ejercicio0414 {

  private char letra;

  Ejercicio0414(char let) {
    letra = let;
    if (let >= 'a') {
      letra -= 'a';
      letra += 'A';
    }
  }

  public char getLetra() {
    return letra;
  }

  public void printLetra() {
    System.out.println(letra);
  }

  public static void main(String argv[]) {
    Ejercicio0414 l1 = new Ejercicio0414('a');
    Ejercicio0414 l2 = new Ejercicio0414('A');
    Ejercicio0414 l3 = new Ejercicio0414('b');
    Ejercicio0414 l4 = new Ejercicio0414('g');
    l1.printLetra();
    l2.printLetra();
    System.out.println(l3.getLetra());
    System.out.println(l4.getLetra());
  }
}
/* EJECUCION:
 A
 A
 B
 G
 */
